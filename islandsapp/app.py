"""IslandsApp module"""
import sys
import typing
from itertools import product


def line_to_list(line: str) -> list[str]:
    """
    Convert a string into a list of strings.

    Args:
        line (str): The string to convert.
    Returns:
        list: list of characters"""

    return list(line.strip())

directions = list(product([-1, 0, 1], repeat=2))
directions.remove((0, 0))


def set_zeros(grid: list, row: int, col: int, file: typing.IO):
    """
    Set the values of a island to 0.

    Args:
        grid (list): The grid to set.
        row (int): The row index.
        col (int): The column index."""

    if (0 <= row < len(grid)) and (0 <= col < len(grid[0])) and (grid[row][col] == "1"):
        if row == len(grid) - 1:
            if line := file.readline():
                grid.append(line_to_list(line))
        grid[row][col] = "0"
        for row_inc, col_inc in directions:
            set_zeros(grid, row + row_inc, col + col_inc, file)


def island_function(file_path: str = "") -> int:
    """Function to count and return the number of islands
    Args:
        file_path (str, optional): path to file. Defaults to "".
    Returns:
        int: number of islands in the file"""

    islands_count = 0
    row = 0
    grid = []
    with open(file_path, "r", encoding="utf8") as file:
        while True:
            if line := file.readline():
                row_list = line_to_list(line)
                grid.append(row_list)
            if row == len(grid):
                break
            for col in range(len(grid[row])):
                if grid[row][col] == "1":
                    islands_count += 1
                    set_zeros(grid, row, col, file)
            row += 1
    return islands_count


if __name__ == "__main__":
    if len(sys.argv) > 1:
        print(island_function(file_path=sys.argv[1]))
    else:
        print("Please provide a file path as parameter to the island app when starting.",
              file=sys.stderr)
