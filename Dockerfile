FROM python:3.10.5-slim-buster
WORKDIR /app
RUN apt-get update -y && \
    apt-get upgrade -y && \
    /usr/local/bin/python -m pip install --upgrade pip && \
    pip install --upgrade pip
COPY requirements.txt .
RUN pip install -U pip && pip install -r requirements.txt
COPY script.sh .
RUN chmod +x script.sh
COPY examples/ examples/
COPY islandsapp/ islandsapp/

CMD ["./script.sh", "/app/examples/1.txt"]