"""Test Functions"""

import pytest

from islandsapp.app import line_to_list, island_function

@pytest.mark.parametrize('text, expected', [
("11001100", ["1", "1", "0", "0", "1", "1", "0", "0"]),
("", []),
('1', ["1"]),
])
def test_line_to_list(text: str, expected: list[str]):
    """Convert a string to a list of characters.

    Args:
        text (str): sample text
        expected (list[str]): list of characters
    """
    assert line_to_list(text) == expected


@pytest.mark.parametrize('path, expected', [
("tests/texamples/project_example.txt", 4),
("tests/texamples/island_in_island_example.txt", 2),
("tests/texamples/no_water_example.txt", 1),
("tests/texamples/no_land_example.txt", 0),
])
def test_island_function(path: str, expected: int):
    """Test main functionality of a program.

    Args:
        path (str): path to test sample with islands grid
        expected (int): expected number of islands
    """
    assert island_function(path) == expected
